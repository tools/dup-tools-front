import * as wasm from "dup-tools-wasm";

// Document type constants
const ANY_DOC_TYPE = 0;
const TX_V10 = 1;
const IDTY_V10 = 2;
const MS_V10 = 3;
const CERT_V10 = 4;
const REVOC_V10 = 5;
const PEER_V11 = 6;
const HEAD_V3 = 7;
const DOC_TYPE_OPTIONS = ["Any document", "Transaction V10", "Identity V10", "Membership V10", "Certification V10", "Revocation V10"/*, "Peer V11", "HEAD V3"*/];
const DOCUMENTS_TYPES_COUNT = DOC_TYPE_OPTIONS.length-1;

// Check result constants
const CHECK_RESULT_UNKNOWN_DOC_TYPE = 0;
const CHECK_RESULT_WRONG_FORMAT = 1;
const CHECK_RESULT_INVALID_SIG = 2;
const CHECK_RESULT_VALID_SIG = 3;

// Get elements
const doc_type = document.getElementById('doc_type');
const get_example = document.getElementById('get_example');
const textarea_left = document.getElementById('textarea_left');
const verify_button = document.getElementById('verify_button');
const textarea_right = document.getElementById('textarea_right');
const check_result = document.getElementById('check_result');
const parse_err = document.getElementById('parse_err');

// Create doc_type options
create_doc_type_options();

// Get documents examples
const doc_examples = [];
for (let i = 1; i <= DOCUMENTS_TYPES_COUNT; i++) {
    doc_examples[i] = wasm.example_doc(i);
}

// Create listeners
verify_button.addEventListener("click", check_input);
doc_type.addEventListener("change", () => {
    if (doc_type.value != ANY_DOC_TYPE) {
        get_example.disabled = false;
    } else {
        get_example.disabled = true;
    }
});
get_example.addEventListener("click", () => {
    if (doc_type.value != ANY_DOC_TYPE) {
        textarea_left.value = doc_examples[doc_type.value];
    } else {
        get_example.disabled = true;
    }
    //console.log(textarea_left.value);
});


// Create document type options
function create_doc_type_options() {
    //Create and append the options
    for (let i = 0; i < DOC_TYPE_OPTIONS.length; i++) {
        let option = document.createElement("option");
        option.value = i.toString();
        option.text = DOC_TYPE_OPTIONS[i];
        doc_type.appendChild(option);
    }
}

// Check user input
function check_input() {
    const input = textarea_left.value;
    if (input.length > 0) {
        const result = wasm.parse_doc_and_verify(input, doc_type.value);
        let output = wasm.parse_doc_into_json(input, doc_type.value);

        // Log for debug
        console.log("input="+input);
        console.log("result="+result);
        console.log("output="+output);

        // Clear all texts
        textarea_right.value = "";
        check_result.innerText = "";
        parse_err.innerText = "";

        switch (result) {
            case CHECK_RESULT_UNKNOWN_DOC_TYPE:
                check_result.style.color = 'red';
                check_result.innerText = "Format : Invalid \u2716";
                parse_err.innerText = "Specify expected document type for more informations !"
                break;
            case CHECK_RESULT_WRONG_FORMAT:
                check_result.style.color = 'red';
                check_result.innerText = "Format : Invalid \u2716";
                breakText(output, parse_err);
                break;
            case CHECK_RESULT_INVALID_SIG:
            check_result.style.color = 'orange';
                prettyPrintJson(output, textarea_right);
                check_result.innerHTML = "Format: Valid \u2714<br>Signature: Invalid \u2716";
                break;
            case CHECK_RESULT_VALID_SIG:
            check_result.style.color = 'green';
                prettyPrintJson(output, textarea_right);
                check_result.innerHTML = "Format: Valid \u2714<br>Signature: Valid \u2714";
                break;
        }
    }
}

function prettyPrintJson(str, elem) {
    let obj = JSON.parse(str);
    let pretty = JSON.stringify(obj, undefined, 4);
    elem.value = pretty;
}

/**
 * Replaces string new lines with `<br>`
 * @see {@link https://stackoverflow.com/q/863779/3841699}
 * @see {@link https://stackoverflow.com/a/1761086/3841699}
 * @type {string} str
 * @type {HTMLElement} elem
 */
function breakText(str, elem) {
    let newLineRegex = /(\n|\n|\r)/gm
    return str
        .split(newLineRegex)
        .forEach(function (part) {
            if (part.match(newLineRegex)) {
                let br = document.createElement('br')
                elem.appendChild(br)
            } else {
                elem.innerText += part
            }
        })
}
