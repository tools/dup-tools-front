[![pipeline status](https://git.duniter.org/tools/dup-tools-front/badges/master/pipeline.svg)](https://git.duniter.org/tools/dup-tools-front/commits/master)

# DUP Tools front  ([try online demo](https://dup-tools-front.duniter.io/))

Based on the [create-wasm-app] template to provide a full js interface using [dup-tools-wasm], a toolbox for developers wishing to use the DUP procotole.

Currently, only the document validator feature is implemented, but the `dup-tools-wasm` library already allows you to do potentially much more !

[create-wasm-app]: https://github.com/rustwasm/create-wasm-app.git
[dup-tools-wasm]: https://git.duniter.org/tools/dup-tools-wasm

## Supported browsers

* Firefox
* Chrome/Chromium

## Usage

You can use DUP Tools directly on the web:  [https://dup-tools-front.duniter.io/](https://dup-tools-front.duniter.io/).



For local use, you only need `npm` :

```
git clone https://git.duniter.org/tools/dup-tools-front
cd dup-tools-front
npm install
npm start
```

Then open `localhost:8080` in a supported browser.

### Change port

If you are already using port `8080` for your developments, you can change the port by modifying `package.json` file as follows :

In the `scripts` section, modify the `start` command:

Replace `"start": "webpack-dev-server"` by `"start": "webpack-dev-server --port NEW_PORT"`
